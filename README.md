# LAI estimation with gap fraction theory for phenotyping image data

This repository provides code examples to estimate leaf area index (LAI) based on phenotyping images.

**Manuscript title**: Extracting leaf area index using viewing geometry effects—A new perspective on high-resolution unmanned aerial system photography 

**Manuscript details**:
Roth, Lukas, Aasen, Helge, Walter, Achim, and Frank Liebisch. 2018. Extracting leaf area index using viewing geometry effects—A new perspective on high-resolution unmanned aerial system photography. ISPRS Journal of Photogrammetry and Remote Sensing.
(https://doi.org/10.1016/j.isprsjprs.2018.04.012).

## Quick start

See `Simple_Example.R` for a quick example code how to estimate LAI based on multi-view data.

## Repository structure

**Data**
 - ground_truth: Manual measurements of LAI
   - *_plots.csv: Experimental design with genotype names
   - *_plants_per_m.csv: Manually counted plants per meter
   - *_true_LAI.csv: LAI determined by imaging leaves with image station 
   - *_LAI_meter.csv: LiCor LAI-2200 measurements
   - *_biomass.csv: Measured dry biomass
   - *_gravimentric_LAI.csv: Estimated LAI based on relation biomass and LAI
 - remote_sensing: Projected visible leaf area based on drone images
   - PA_p.csv: Projected leaf area per image
   - viewpoint.csv: Viewpoint information for images (e.g., azimuth and zenith angle)
 - simulation: Simulated data
   - CC.csv: Projected leaf area per simulated image
   - camera_position_all.csv: Simulated viewpoint information 

**Code**
  - `Simple_Example.R`: Example code to estimate LAI based on multi-view data
  - `Full_Analysis.R`: Full analysis of the data according to published paper
  - subscripts: Folder with auxiliary R scripts

**Software requirement**:
 - Operation system: Any
 - R version: 4.1.1
